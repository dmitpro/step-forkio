# Step project Forkio


### Our Team

- [Alexander Makar](https://gitlab.com/AlexMakar "Alexander Makar")
- [Dmytro Protsenko](https://gitlab.com/dmitpro "Dmytro Protsenko")

------------

### We Used:

##### gulp for development and build:
- browser-sync
- gulp
- gulp-autoprefixer
- gulp-clean
- gulp-clean-css
- gulp-concat
- gulp-imagemin
- gulp-sass
- gulp-uglify


##### methodology BEM in the class names
##### gitlab pages for deploy: [Step project Forkio](https://dmitpro.gitlab.io/step-forkio/) 

------------
| Developer  |  Tasks |
| ------------ | ------------ |
| Alexander Makar  | Header, header menu, dropdown menu, section "People Are Talking About Fork" |
| Dmytro Protsenko  | Section "Revolutionary Editor", section "Here is what you get", section "Fork Subscription Pricing", gulpfile, gitlab pages  |

